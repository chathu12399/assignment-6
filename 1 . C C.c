#include <stdio.h>

void pattern(int n);
void line(int a);
int i=1; // Number of lines

void pattern(int n)
{
    if(n>0)
    {
        line(i);
        printf("\n");
        ++i;
        pattern(n-1);
    }
}

void line(int a)
{
    if(a>0)
    {
        printf("%d", a);
        line(a-1);
    }
}

int main()
{
    int r;
    printf("Enter the number of rows that you want: ");
    scanf("%d", &r);
    pattern(r);
    return 0;
}
